# ApiAtenea

Ing Cristhian Caycedo


VIDEO

https://universidadeaneduco-my.sharepoint.com/:f:/g/personal/ccaycedo2_universidadean_edu_co/EoqhxYMF_79JpWQ56YR-HbEBYzgOSIkfmWuYbxdqL1Ibgg?e=IZHpDS


![Estructura](./imagenes/pirncipal.png)


Diseñar una API RESTFul1 que permite administrar los activos, esta deberá permitir:
• Buscar por todos los activos.
• Buscar los activos por: tipo, fecha de compra, serial.
• Crear nuevos activos.
• Actualizar activos.
o Solo se permite cambiar el serial interno y fecha de baja.
• Listar las áreas o personas.