-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-01-2024 a las 00:33:27
-- Versión del servidor: 10.4.32-MariaDB
-- Versión de PHP: 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `activosfijos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activofijo`
--

CREATE TABLE `activofijo` (
  `id` int(11) NOT NULL,
  `alto` int(11) DEFAULT NULL,
  `ancho` int(11) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `fechabaja` datetime DEFAULT NULL,
  `fechacompra` datetime DEFAULT NULL,
  `fechacreacion` datetime DEFAULT NULL,
  `fechamodificacion` datetime DEFAULT NULL,
  `largo` int(11) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `numerointerno` int(11) DEFAULT NULL,
  `peso` int(11) DEFAULT NULL,
  `serial` int(11) DEFAULT NULL,
  `usuariocreacion` varchar(255) DEFAULT NULL,
  `usuariomodificacion` varchar(255) DEFAULT NULL,
  `valorcompra` int(11) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `activofijo`
--

INSERT INTO `activofijo` (`id`, `alto`, `ancho`, `color`, `descripcion`, `estado`, `fechabaja`, `fechacompra`, `fechacreacion`, `fechamodificacion`, `largo`, `nombre`, `numerointerno`, `peso`, `serial`, `usuariocreacion`, `usuariomodificacion`, `valorcompra`, `tipo`) VALUES
(1, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '5'),
(2, NULL, NULL, NULL, NULL, NULL, '2024-01-03 17:06:03', NULL, NULL, NULL, NULL, '', NULL, NULL, 2222222, NULL, NULL, NULL, NULL),
(3, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, 22211, '3', '3', NULL, '55'),
(4, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(5, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(6, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(7, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(8, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(9, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(10, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(11, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(12, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(13, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(14, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(15, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(16, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55'),
(17, 7, 7, '7', '7', '7', '2024-01-03 17:06:03', '2024-01-17 17:06:03', NULL, NULL, NULL, '3', NULL, NULL, NULL, '3', '3', NULL, '55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activofijosolicitante`
--

CREATE TABLE `activofijosolicitante` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `fechacreacion` datetime DEFAULT NULL,
  `fechamodificacion` datetime DEFAULT NULL,
  `fechasolicitud` datetime DEFAULT NULL,
  `idactivofijo` int(11) DEFAULT NULL,
  `idsolicitante` int(11) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `usuariocreacion` varchar(255) DEFAULT NULL,
  `usuariomodificacion` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitante`
--

CREATE TABLE `solicitante` (
  `id` int(11) NOT NULL,
  `area` varchar(255) DEFAULT NULL,
  `cargo` varchar(255) DEFAULT NULL,
  `celular` int(11) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fechacreacion` datetime DEFAULT NULL,
  `fechamodificacion` datetime DEFAULT NULL,
  `identificacion` varchar(255) DEFAULT NULL,
  `nombre_completo` varchar(255) DEFAULT NULL,
  `usuariocreacion` varchar(255) DEFAULT NULL,
  `usuariomodificacion` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activofijo`
--
ALTER TABLE `activofijo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `activofijosolicitante`
--
ALTER TABLE `activofijosolicitante`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `solicitante`
--
ALTER TABLE `solicitante`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activofijo`
--
ALTER TABLE `activofijo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `activofijosolicitante`
--
ALTER TABLE `activofijosolicitante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `solicitante`
--
ALTER TABLE `solicitante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
