export class solicitante
{
	public id: number;
	public area: string;
	public cargo: string;
	public celular: number;
	public ciudad: string;
	public email: string;
	public fechacreacion: Date;
	public fechamodificacion: Date;
	public identificacion: string;
	public nombre_completo: string;
	public usuariocreacion: string;
	public usuariomodificacion: string;
}
