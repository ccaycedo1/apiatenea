package gov.co.atenea.api.model;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
public class activofijo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	
	@Column
	public String nombre;
	
	@Column
	public String descripcion;
	
	@Column
	public String tipo;
	
	@Column
	public Integer serial;
	
	@Column
	public Integer numerointerno;
	
	@Column
	public Integer peso;
	
	@Column
	public Integer alto;
	
	@Column
	public Integer ancho;
	
	@Column
	public Integer largo;
	
	@Column
	public Integer valorcompra;
	
	@Column
	public Timestamp fechacompra;
	
	@Column
	public Timestamp fechabaja;
	
	@Column
	public String estado;
	
	@Column
	public String color;
	
	@Column
	public String usuariocreacion;
	
	@Column
	public Timestamp fechacreacion;
	
	@Column
	public String usuariomodificacion;
	
	@Column
	public Timestamp fechamodificacion;
	
	
}
