package gov.co.atenea.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;

import gov.co.atenea.api.model.activofijo;

@Service
public class ActivosFijosServiceImpl implements ActivosFijosService{
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<activofijo> listar() {
		Query query = em.createNativeQuery("SELECT * FROM activofijo",activofijo.class);
		return query.getResultList();
	}

	@Override
	public List<activofijo> buscar(activofijo dto) {
	    CriteriaBuilder cb = em.getCriteriaBuilder();
	    CriteriaQuery<activofijo> cq = cb.createQuery(activofijo.class);
	    Root<activofijo> root = cq.from(activofijo.class);
	    
	    List<Predicate> predicates = new ArrayList<>();

	    if (dto.tipo != null) {
	        predicates.add(cb.equal(root.get("tipo"), dto.tipo));
	    }

	    if (dto.fechacompra != null) {
	        predicates.add(cb.equal(root.get("fechacompra"), dto.fechacompra));
	    }

	    if (dto.serial != null) {
	        predicates.add(cb.equal(root.get("serial"), dto.serial));
	    }

	    cq.where(predicates.toArray(new Predicate[0]));

	    TypedQuery<activofijo> query = em.createQuery(cq);
	    return query.getResultList();
	}
	
}
