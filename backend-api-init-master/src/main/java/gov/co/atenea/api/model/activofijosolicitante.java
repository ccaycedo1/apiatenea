package gov.co.atenea.api.model;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
public class activofijosolicitante {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	
	@Column
	public Integer idactivofijo;
	
	@Column
	public Integer idsolicitante;
	
	@Column
	public Timestamp fechasolicitud;
	
	@Column
	public Integer cantidad;
	
	@Column
	public String observaciones;
	
	@Column
	public String usuariocreacion;
	
	@Column
	public Timestamp fechacreacion;
	
	@Column
	public String usuariomodificacion;
	
	@Column
	public Timestamp fechamodificacion;
	
	
}
