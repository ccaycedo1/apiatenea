package gov.co.atenea.api.model;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
public class solicitante {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	
	@Column
	public String nombreCompleto;
	
	@Column
	public String identificacion;
	
	@Column
	public Integer celular;
	
	@Column
	public String area;
	
	@Column
	public String ciudad;
	
	@Column
	public String email;
	
	@Column
	public String cargo;
	
	@Column
	public String usuariocreacion;
	
	@Column
	public Timestamp fechacreacion;
	
	@Column
	public String usuariomodificacion;
	
	@Column
	public Timestamp fechamodificacion;
	
	
}
