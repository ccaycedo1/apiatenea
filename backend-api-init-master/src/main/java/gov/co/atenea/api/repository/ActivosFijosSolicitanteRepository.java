package gov.co.atenea.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import gov.co.atenea.api.model.activofijosolicitante;
import gov.co.atenea.api.service.ActivosFijosSolicitanteService;

@Repository
public interface ActivosFijosSolicitanteRepository extends JpaRepository<activofijosolicitante, Integer>, ActivosFijosSolicitanteService{
}
