package gov.co.atenea.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.co.atenea.api.model.activofijo;
import gov.co.atenea.api.model.solicitante;
import gov.co.atenea.api.repository.ActivosFijosRepository;
import gov.co.atenea.api.repository.SolicitanteRepository;

@RestController
@RequestMapping("activosfijos")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ActivosFijosRest {

	@Autowired
	private ActivosFijosRepository dao;
	
	@Autowired
	private SolicitanteRepository dao1;

	@GetMapping("/listar")
	public ResponseEntity<?>  listar() {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(dao.listar()); 
		} catch (Exception e) {			
			return ResponseEntity.badRequest().body("Error en la peticion."); 
		}
	}

	@PostMapping("/buscar")
	public ResponseEntity<?> buscar(@RequestBody activofijo dto) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(dao.buscar(dto)); 
		} catch (Exception e) {			
			return ResponseEntity.badRequest().body("Datos faltantes para buscar un activo."); 
		}

	}

	@PostMapping("/crear")
	public ResponseEntity<?> crear(@RequestBody activofijo dto) {
		try {
			activofijo dto1 = dao.save(dto);
			if (dto1 != null) {
				return ResponseEntity.status(HttpStatus.CREATED).body(dto1); // 201 para creación exitosa
			} else {
				return ResponseEntity.badRequest().body("Datos faltantes para crear un activo."); 
			}
		} catch (Exception e) {
			
			return ResponseEntity.badRequest().body("Datos faltantes para crear un activo."); 
		}

	}

	@PostMapping("/actualizar")
	public ResponseEntity<?> actualizar(@RequestBody activofijo dto) {
		try {
			// Actualizar activos. Solo se permite cambiar el serial interno y fecha de baja.
			activofijo clone = dao.findById(dto.id).orElse(dto);
			clone.id = dto.id;
			clone.serial = dto.serial;
			clone.fechabaja = dto.fechabaja;
			return ResponseEntity.status(HttpStatus.CREATED).body(dao.save(clone)); // 201 para creación exitosa
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("Error actualizando un activo.");
		}

	}

	@GetMapping
	public String test() {
		return "Controlador activos";
	}
	
	@PostMapping("/crearSol")
	public ResponseEntity<?> crearSol(@RequestBody solicitante dto) {
		try {
			solicitante dto1 = dao1.save(dto);
			if (dto1 != null) {
				return ResponseEntity.status(HttpStatus.CREATED).body(dto1); // 201 para creación exitosa
			} else {
				return ResponseEntity.badRequest().body("Datos faltantes para crear un solicitante."); 
			}
		} catch (Exception e) {
			
			return ResponseEntity.badRequest().body("Datos faltantes para crear un solicitante."); 
		}

	}

}
