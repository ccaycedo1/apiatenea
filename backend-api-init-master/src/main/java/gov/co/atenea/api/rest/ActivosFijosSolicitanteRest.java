package gov.co.atenea.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.co.atenea.api.repository.ActivosFijosSolicitanteRepository;

@RestController
@RequestMapping("activosfijossolicitante")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ActivosFijosSolicitanteRest {

	@Autowired
	private ActivosFijosSolicitanteRepository dao;
	
	@GetMapping("/listar")
	public ResponseEntity<?>  listar() {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(dao.findAll()); 
		} catch (Exception e) {			
			return ResponseEntity.badRequest().body("Error en la peticion."); 
		}
	}

}
