package gov.co.atenea.api.service;

import java.util.List;

import gov.co.atenea.api.model.activofijo;


public interface ActivosFijosService {
	
	public List<activofijo> listar();
	public List<activofijo> buscar(activofijo dto);
	
	
}
