package gov.co.atenea.api.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.co.atenea.api.model.solicitante;
import gov.co.atenea.api.repository.SolicitanteRepository;

@RestController
@RequestMapping("solicitante")
@CrossOrigin(origins = "*", maxAge = 3600)
public class Solicitante {
	
	@Autowired
	private SolicitanteRepository dao;
		
	@GetMapping
    public String test() {
        return "Controlador solicitante";
    }
	
	@GetMapping("/listar")
	public ResponseEntity<?> listar(){
		List<solicitante> data= dao.findAll();
		
		if(data.size() >=1) {
			return ResponseEntity.ok(dao.findAll());
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No hay datos "); // 404 para búsquedas sin resultados
		}
			
		
	}
	
	@PostMapping("/crear")
	public ResponseEntity<?> crear(@RequestBody solicitante dto) {
		try {
			solicitante dto1 = dao.save(dto);
			if (dto1 != null) {
				return ResponseEntity.status(HttpStatus.CREATED).body(dto1); // 201 para creación exitosa
			} else {
				return ResponseEntity.badRequest().body("Datos faltantes para crear un solicitante."); 
			}
		} catch (Exception e) {
			
			return ResponseEntity.badRequest().body("Datos faltantes para crear un solicitante."); 
		}

	}
	
	

}
