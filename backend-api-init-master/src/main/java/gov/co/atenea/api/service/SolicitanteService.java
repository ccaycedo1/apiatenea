package gov.co.atenea.api.service;

import java.util.List;

import gov.co.atenea.api.model.activofijo;
import gov.co.atenea.api.model.solicitante;


public interface SolicitanteService {
	
	public List<solicitante> listar();
	
	
}
