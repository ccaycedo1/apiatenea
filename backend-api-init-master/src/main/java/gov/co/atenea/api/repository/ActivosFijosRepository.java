package gov.co.atenea.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import gov.co.atenea.api.model.activofijo;
import gov.co.atenea.api.service.ActivosFijosService;

@Repository
public interface ActivosFijosRepository extends JpaRepository<activofijo, Integer>, ActivosFijosService{
}
